#!/usr/bin/env bash

function retry_failed_rspec_examples() {
  exit_with_allowed_failure_exit_code
}

function echoinfo {
  echo $1
}


# exit with an allowed_failure exit code if the flaky test was part of this MR that triggered the pipeline
function exit_with_allowed_failure_exit_code {
  local changed_files=$(git diff --name-only $CI_MERGE_REQUEST_TARGET_BRANCH_SHA | grep spec)
  echoinfo "A test was flaky and succeeded after being retried. Checking to see if flaky test is part of this MR..."

  if [[ "$changed_files" == "" ]]; then
    echoinfo "Flaky test was not part of this MR."
    return
  fi

  echo "$changed_files" | while read changed_file
  do
    echoinfo "Searching flaky tests for ${changed_file}"
    if grep -q "$changed_file" "$RETRIED_TESTS_REPORT_PATH"; then
      echoinfo "Exiting with code 137 because the flaky test was part of this MR."
      exit 137
    fi
  done
  echoinfo "Flaky test was not part of this MR."
}

retry_failed_rspec_examples
